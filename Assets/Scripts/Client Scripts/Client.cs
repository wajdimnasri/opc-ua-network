﻿using Opc.Ua;
using Opc.Ua.Client;
using Opc.Ua.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class Client : MonoBehaviour
{
    #region initialization
    // Use this for initialization
    public Text Txt_UrlServer;
    public Text Txt_Browse_Server;
    public Text Txt_NodeId;
    public Text Txt_Node_Value;
    public Text Txt_NewValue;
    public Text Txt_lengthstring;
    public Text Txt_MethodId;
    public Text Txt_MonitoredNodeId;

    [SerializeField] private Button m_ClearMessages;
    [SerializeField] private Scrollbar m_Scrollbar;
    Session session;


    // display the subscription info
    List<MonitoredItem> MonitoredItems = new List<MonitoredItem>();
    List<DataValue> DataValues = new List<DataValue>();
    bool IfNewSubscriptionValue = false;
    uint subscriptionMessageId;
    #endregion


    private void Update()
    {
        RefreshSubscriptionDisplay();
    }


    // Connect to the Server
    #region Connect
    public void Connect()
    {
        Connect(Txt_UrlServer.text).Wait();
    }
    private async Task Connect(string endpointURL)
    {
        Debug.Log("1 - Create an Application Configuration.");
        // Create an Application Configuration
        ApplicationConfiguration config = new ApplicationConfiguration()
        {
            ApplicationName = "MyHomework",
            ApplicationType = ApplicationType.Client,
            SecurityConfiguration = new SecurityConfiguration
            {
                ApplicationCertificate = new CertificateIdentifier { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\MachineDefault", SubjectName = Utils.Format(@"CN={0}, DC={1}", "MyHomework", "System.Net.Dns.GetHostName()") },
                TrustedIssuerCertificates = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\UA Certificate Authorities" },
                TrustedPeerCertificates = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\UA Applications" },
                RejectedCertificateStore = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\RejectedCertificates" },
                AutoAcceptUntrustedCertificates = true,
                AddAppCertToTrustedStore = true
            },
            TransportConfigurations = new TransportConfigurationCollection(),
            TransportQuotas = new TransportQuotas { OperationTimeout = 15000 },
            ClientConfiguration = new ClientConfiguration { DefaultSessionTimeout = 60000 },
            TraceConfiguration = new TraceConfiguration()
        };

        // validate app's config
        await config.Validate(ApplicationType.Client);
        if (config.SecurityConfiguration.AutoAcceptUntrustedCertificates)
        {
            config.CertificateValidator.CertificateValidation += (s, e) => { e.Accept = (e.Error.StatusCode == StatusCodes.BadCertificateUntrusted); };
        }

        // create a new application with previous config
        ApplicationInstance application = new ApplicationInstance
        {
            ApplicationName = "OPC UA Client",
            ApplicationType = ApplicationType.Client,
            ApplicationConfiguration = config,
            ConfigSectionName = Utils.IsRunningOnMono() ? "Opc.Ua.MonoClient" : "Opc.Ua.Client"
        };

        // if there is no previous Certificate, create a new one
        application.CheckApplicationInstanceCertificate(false, 2048).GetAwaiter().GetResult();

        Debug.Log("2 - Discover endpoints of " + endpointURL);
        EndpointDescription selectedEndpoint = CoreClientUtils.SelectEndpoint(endpointURL, false, 15000);

        // get end points from server
        //var selectedEndpoint = CoreClientUtils.SelectEndpoint("opc.tcp://" + Dns.GetHostName() + ":48010", useSecurity: true, operationTimeout: 15000);

        Debug.Log("3 - Create a session with OPC UA server.");
        var endpointConfiguration = EndpointConfiguration.Create(config);
        var endpoint = new ConfiguredEndpoint(null, selectedEndpoint, endpointConfiguration);
        
        session = await Session.Create(config, endpoint, false, "OPC UA Console Client", 60000, null, null);
    }
    #endregion

    // Browse the Server
    #region BrowseServer
    public void BrowseServer()
    {
        ReferenceDescriptionCollection references;
        Byte[] continuationPoint;

        references = session.FetchReferences(ObjectIds.ObjectsFolder);

        session.Browse(
            null,
            null,
            ObjectIds.ObjectsFolder,
            0u,
            BrowseDirection.Forward,
            ReferenceTypeIds.HierarchicalReferences,
            true,
            (uint)NodeClass.Variable | (uint)NodeClass.Object | (uint)NodeClass.Method,
            out continuationPoint,
            out references);

        Debug.Log(" DisplayName, BrowseName, NodeClass");

        foreach (var rd in references)
        {
            //Debug.Log("Display Name : " + rd.DisplayName + " Browse Name : " + rd.BrowseName + " NodeClass: " + rd.NodeClass);
            Txt_Browse_Server.text += "Display Name: " + rd.DisplayName + " Browse Name : " + rd.BrowseName + " NodeClass: " + rd.NodeClass;

            ReferenceDescriptionCollection nextRefs;
            byte[] nextCp;
            session.Browse(
                null,
                null,
                ExpandedNodeId.ToNodeId(rd.NodeId, session.NamespaceUris),
                0u,
                BrowseDirection.Forward,
                ReferenceTypeIds.HierarchicalReferences,
                true,
                (uint)NodeClass.Variable | (uint)NodeClass.Object | (uint)NodeClass.Method,
                out nextCp,
                out nextRefs);

            foreach (var nextRd in nextRefs)
            {
                //Debug.Log("Display Name : "+ nextRd.DisplayName+ " Browse Name : "+ nextRd.BrowseName+ " NodeClass: " +nextRd.NodeClass);
                Txt_Browse_Server.text += "Display Name: " + nextRd.DisplayName + " Browse Name : " + nextRd.BrowseName + " NodeClass: " + nextRd.NodeClass;
            }
        }
    }
    #endregion

    //Write Value To Node
    #region WriteValueToNode
    // The simple Write from a node with simple value
    private void WriteValue()
    {
        StatusCodeCollection statuscode = null;
        DiagnosticInfoCollection info = null;
        try
        {
            DataValue valueToWrite = new DataValue();
            valueToWrite.Value = Txt_NewValue.text;// Int32.Parse(Txt_NewValue);
            List<WriteValue> writelist = new List<WriteValue>();
            writelist.Add(new WriteValue()
            {
                NodeId = Txt_NodeId.text,// ns=3;i=10227
                AttributeId = Attributes.Value,
                Value = valueToWrite
            });
            //Code to write on Node
            session.Write(new RequestHeader() { },
                new WriteValueCollection(writelist),
                out statuscode, out info);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }
    // The simple Write from a node with array value (Xml Document)
    private void WriteLongValue(string[] value)
    {
        StatusCodeCollection statuscode = null;
        DiagnosticInfoCollection info = null;
        try
        {
            DataValue valueToWrite = new DataValue();
            valueToWrite.Value = value;// Int32.Parse(Txt_NewValue);
            List<WriteValue> writelist = new List<WriteValue>();
            writelist.Add(new WriteValue()
            {
                NodeId = Txt_NodeId.text,// ns=3;i=10227
                AttributeId = Attributes.Value,
                Value = valueToWrite
            });
            //Code to write on Node
            session.Write(new RequestHeader() { },
                new WriteValueCollection(writelist),
                out statuscode, out info);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }
    #endregion

    // Read simple Node value
    #region Read Value from a node server
    public void ReadValue()
    {
        NodeId nodeId = new NodeId(Txt_NodeId.text);
        DataValue Result = session.ReadValue(nodeId);
        Debug.Log("Read result = " + Result.Value.ToString());
        Txt_Node_Value.text = Result.Value.ToString();
    }
    #endregion

    //MonitoredItem after subscription
    #region Subscription
    //Create a subscription
    public void CreateSubscription()
    {
        Debug.Log("5 - Create a subscription with publishing interval of 1 second.");
        var subscription = new Subscription(session.DefaultSubscription) { PublishingInterval = 1000 };
        // Add a list of items to the subscription
        AddList(subscription);
        session.AddSubscription(subscription);
        subscription.Create();
    }
    // Add a list of MonitoredItem to the subscription
    private void AddList(Subscription subscription)
    {
        Debug.Log("6 - Add a list of items (server current time and status) to the subscription.");
        var list = new List<MonitoredItem> {
                new MonitoredItem(subscription.DefaultItem)
                {
                    DisplayName = "MonitoredItem", StartNodeId = Txt_MonitoredNodeId.text   //StartNodeId = "i="+Variables.Server_ServerStatus_CurrentTime.ToString()
                }
            };
        list.ForEach(i => i.Notification += OnNotification);
        subscription.AddItems(list);
    }
    

    private void OnNotification(MonitoredItem item, MonitoredItemNotificationEventArgs e)
    {
        foreach (var value in item.DequeueValues())
        {
            // *** do not call other method from mono ***
            // can not pass objects directly into another method in InterfaceManager, 
            // cause the other method would also use the same thread that this method is using,
            // which is not main thread. If it is not main thread, you can't use UnityEngine's API.
            MonitoredItems.Add(item);
            DataValues.Add(value);
            IfNewSubscriptionValue = true;
        }
    }
    
    // refresh subscription state
    void RefreshSubscriptionDisplay()
    {
        if (IfNewSubscriptionValue)
        {

            for (int i = 0; i < DataValues.Count; i++)
            {
                subscriptionMessageId++;
                string subscriptionInfo =
                    subscriptionMessageId + "\t" +
                    MonitoredItems[i].DisplayName + "\t" +
                    MonitoredItems[i].StartNodeId + "\t" +
                    DataValues[i].SourceTimestamp.ToString() + "\t" +
                    DataValues[i].StatusCode.ToString() + "\t" +
                    DataValues[i].Value.ToString() + "\n";
                Txt_Browse_Server.text += subscriptionInfo;
            }

            IfNewSubscriptionValue = false;

            //clean the lists so next time the old message won't be displayed again.
            MonitoredItems.Clear();
            DataValues.Clear();
        }

    }
    #endregion

    // Select xml Document and uplod the data to string value node in OPC UA
    #region XML Methods exchange
    public void UploadXml()
    {
        String[] xmlText = File.ReadAllLines("Assets/XMLDocuments/DocumentInput.xml");
        foreach (string value in xmlText)
        {
            Debug.Log(value);
        }
        WriteLongValue(xmlText);
    }
    // Read string value from a node OPC UA and create xml file
    public void ReadSaveXml()
    {
        //Read fromNodeId
        NodeId nodeId = new NodeId(Txt_NodeId.text);
        DataValue Result = session.ReadValue(nodeId);
        File.WriteAllLines("Assets/XMLDocuments/DocumentOutput.xml", (string[])Result.Value);
    }
    #endregion

    // call Methods from the server side
    #region Methods
    // Call my owen method Length from the Server
    public void LengthMethod()
    {
        IList<object> val = session.Call(
        // I know that this node holds the method I like to use
        new NodeId(Txt_NodeId.text),
        // and I know this node is method 
        new NodeId(Txt_MethodId.text),
        Txt_lengthstring.text);
        Txt_Browse_Server.text = (string)val[0];
        Debug.Log(val[0]);
    }

    // clear all messages    
    public void ClearMessage()
    {
        m_Scrollbar.size = 1f;
        m_Scrollbar.value = 0f;
        Txt_Browse_Server.text = "";
    }
    #endregion
}
