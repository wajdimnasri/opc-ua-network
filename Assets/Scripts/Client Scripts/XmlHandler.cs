﻿using UnityEngine;
using System.Xml;
using System.IO;

/// <summary>
/// Another way of converting between XML file and strings.
/// Not being used by current version.
/// </summary>
public class XmlHandler : MonoBehaviour
{

    private void Start()
    {
        CreateXml();

        string str = ConvertXmlToString(CreateXml());
        Debug.Log(str);

        XmlDocument xmlDoc = ConvertStringToXml(str);
        Debug.Log(xmlDoc.InnerText);
        
    }

    public XmlDocument CreateXml()
    {
        XmlDocument xmlDoc = new XmlDocument();

        //declare head
        XmlDeclaration declaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
        xmlDoc.AppendChild(declaration);

        //create root node
        XmlElement root = xmlDoc.CreateElement("StudentList");
        xmlDoc.AppendChild(root);

        //crate new node
        XmlElement student = xmlDoc.CreateElement("Student");
        student.SetAttribute("id", "1");
        XmlElement studentName = xmlDoc.CreateElement("Name");
        XmlText xmlText = xmlDoc.CreateTextNode("John Doe");

        studentName.AppendChild(xmlText);
        student.AppendChild(studentName);
        root.AppendChild(student);

        //save xml
        // will this work under win10 or UWP?
        //xmlDoc.Save(Application.dataPath + "Student.xml");

        return xmlDoc;
    }


    public XmlElement[] CreateXmlElementArray()
    {

        XmlDocument xmlDoc = new XmlDocument();
        //declare head
        XmlDeclaration declaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
        xmlDoc.AppendChild(declaration);

        //create root node
        XmlElement root = xmlDoc.CreateElement("StudentList");
        xmlDoc.AppendChild(root);

        //crate new node
        XmlElement student = CreateElement(xmlDoc, root, "Student");
        XmlElement studentName = CreateElement(xmlDoc, student, "StudentName", "id", "1");
        XmlText xmlText = CreateXmlText(xmlDoc, studentName, "John Doe");

        // create 2nd node
        XmlElement studentName1 = CreateElement(xmlDoc, student, "StudentName", "id", "2");
        XmlText xmlText1 = CreateXmlText(xmlDoc, studentName1, "Jane Doe");

        XmlNodeList nodeList = root.GetElementsByTagName("StudentName");
        int count = nodeList.Count;

        XmlElement[] elementArray = new XmlElement[count];

        for (int i = 0; i < count; i++)
        {
            //string id = ((XmlElement)nodeList[i]).GetAttribute("id");
            //string name = ((XmlElement)nodeList[i]).GetElementsByTagName("StudentName")[0].InnerText;

            elementArray[i] = (XmlElement)nodeList[i];
        }

        return elementArray;
    }


    public void ReadXmlElementArry(XmlElement[] array)
    {
        foreach (XmlElement element in array)
        {
            Debug.Log(element.InnerText.ToString());
        }

        /*
        XmlDocument xmlDoc = new XmlDocument();

        //declare head
        XmlDeclaration declaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
        xmlDoc.AppendChild(declaration);

        //create root node
        XmlElement root = xmlDoc.CreateElement("StudentList");
        xmlDoc.AppendChild(root);

        //crate new node
        XmlElement student = xmlDoc.CreateElement("Student");
        student.SetAttribute("id", "1");
        XmlElement studentName = xmlDoc.CreateElement("Name");
        XmlText xmlText = xmlDoc.CreateTextNode("John Doe");

        studentName.AppendChild(xmlText);
        student.AppendChild(studentName);
        root.AppendChild(student);

        //save xml
        xmlDoc.Save("C:/Yi.Han's Projects/05 OPC UA/02 UA c#/UPC UA 2/OPC Foundation/Assets/Student.xml");
        */
    }


    /// <summary>
    /// create a XmlElement with given name, and append it to the given parent
    /// </summary>
    /// <param name="doc"></param>
    /// <param name="parent"></param>
    /// <param name="elementName"></param>
    /// <returns></returns>
    XmlElement CreateElement(XmlDocument doc, XmlElement parent, string elementName)
    {
        //crate new node
        XmlElement xe = doc.CreateElement(elementName);
        parent.AppendChild(xe);

        return xe;
    }

    /// <summary>
    /// create a XmlElement with given name, then add an attribute with name and content, and append it to the given parent
    /// </summary>
    /// <param name="doc"></param>
    /// <param name="parent"></param>
    /// <param name="elementName"></param>
    /// <param name="attributeName"></param>
    /// <param name="attributeContent"></param>
    /// <returns></returns>
    XmlElement CreateElement(XmlDocument doc, XmlElement parent, string elementName, string attributeName, string attributeContent)
    {
        //crate new node
        XmlElement xe = doc.CreateElement(elementName);
        xe.SetAttribute(attributeName, attributeContent);
        parent.AppendChild(xe);

        return xe;
    }

    /// <summary>
    /// create a XmlText with given name, and append it to the given parent
    /// </summary>
    /// <param name="doc"></param>
    /// <param name="parent"></param>
    /// <param name="text"></param>
    /// <returns></returns>
    XmlText CreateXmlText(XmlDocument doc, XmlElement parent, string text)
    {
        XmlText xmlText = doc.CreateTextNode(text);
        parent.AppendChild(xmlText);

        return xmlText;
    }

    #region XML and string convertion

    /// <summary>
    /// turn XmlDocument into string  
    /// </summary>  
    /// <param name="xmlDoc"></param>  
    /// <returns></returns>  
    public string ConvertXmlToString(XmlDocument xmlDoc)
    {
        MemoryStream stream = new MemoryStream();
        XmlTextWriter writer = new XmlTextWriter(stream, null);
        writer.Formatting = Formatting.Indented;
        xmlDoc.Save(writer);

        // check XmlDeclaration
        XmlNode firstNode = xmlDoc.FirstChild;
        
        // default encoding is UTF8
        System.Text.Encoding encoding = System.Text.Encoding.UTF8;
        // otherwise use xmlDoc's own encoding
        if (firstNode.NodeType == XmlNodeType.XmlDeclaration)
        {
            try
            {
                XmlDeclaration dec = firstNode as XmlDeclaration;
                encoding = System.Text.Encoding.GetEncoding(dec.Encoding);
                Debug.Log("The Encoding of this XmlDocument is " + dec.Encoding);
            }
            catch
            {
                Debug.LogWarning("The Encoding of this XmlDocument is not clear, use UTF8 as default.");
            }
        }
        else
        {
            Debug.LogWarning("There is no declaration in this XmlDocument, converting cancelled!");
            return null;
        }

        StreamReader sr = new StreamReader(stream, encoding);
        stream.Position = 0;
        string xmlString = sr.ReadToEnd();
        sr.Close();
        stream.Close();
        return xmlString;
    }

    /// <summary>
    /// turn a str into XmlDocument, if it is readable
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public XmlDocument ConvertStringToXml(string str)
    {
        if (str == null)
        {
            Debug.LogWarning("Given string can not be Null! Converting cancelled.");
            return null;               
        }
        StringReader Reader = new StringReader(str);

        XmlDocument xmlDoc = new XmlDocument();

        xmlDoc.Load(Reader);

        return xmlDoc;
    }

    #endregion

    #region Send Xml to server
    /// <summary>
    /// call this method, to get the Xml that you want to send to the server
    /// </summary>
    /// <returns></returns>
    public XmlDocument GetXmlToSend()
    {
        XmlDocument doc = CreateXml();

        return doc;
    }

    #endregion
}
