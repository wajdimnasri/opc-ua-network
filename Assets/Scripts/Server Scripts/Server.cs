﻿using System;
using Opc.Ua;
using Opc.Ua.Configuration;
using Opc.Ua.Server.Controls;
using UnityEngine;

namespace Quickstarts.ReferenceServer
{
	public class Server : MonoBehaviour {

        // Use this for initialization
        #region initialization
        ApplicationInstance application;
        #endregion

        // Start Server
        #region Server
        void Start()
        {
          StartServer();
        }

        void StartServer(){
				ApplicationInstance.MessageDlg = new ApplicationMessageDlg();
				application = new ApplicationInstance();
				application.ApplicationType   = ApplicationType.Server;
				application.ConfigSectionName = "Quickstarts.ReferenceServer";
			 try
            {
                // load the application configuration.
                application.LoadApplicationConfiguration(false).Wait();
          
                // check the application certificate.
                bool certOk = application.CheckApplicationInstanceCertificate(false, 0).Result;
                if (!certOk)
                {
                    throw new Exception("Application instance certificate invalid!");
                }

                // start the server.
                application.Start(new ReferenceServer()).Wait();
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
		}
        #endregion

        //Stop running Server after exit application
        #region ApplicationQuit
        void OnApplicationQuit()
       {
           application.Stop();
           Debug.Log("Application ending after " + Time.time + " seconds");
       }
        #endregion
    }
}
